import './App.css';
import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendConference from './AttendConference';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import PresentationForm from './PresentationForm';
import MainPage from './MainPage';


function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />

      <Routes>
        <Route index element={<MainPage />} />
        <Route path="locations">
          <Route path="new" element={<LocationForm />} />
        </Route>
        <Route path="conferences">
          <Route path="new" element={<ConferenceForm />} />
        </Route>
        <Route path="presentations">
          <Route path="new" element={<PresentationForm />} />
        </Route>
        <Route path="attendees">
          <Route path="new" element={<AttendConference />} />
          <Route path="" element={<AttendeesList attendees={props.attendees} />} />
        </Route>

        {/* <Route path="conferences/new" element={<ConferenceForm />} />
            <Route path="attendees/new" element={<AttendConference />} /> */}
        {/* <Route path="locations/new" element={<LocationForm />} /> */}


      </Routes>

    </BrowserRouter>
  );
}

//   return (
//     <>
//       <Nav />
//       <div className="container">
//         <AttendConference />
//         {/* <ConferenceForm /> */}
//         {/* <LocationForm /> */}
//         {/* <AttendeesList attendees={props.attendees} /> */}
//       </div>
//     </>
//   );
// }



export default App;
