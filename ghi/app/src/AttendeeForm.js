import React, { useEffect, useState } from 'react';

function AttendeeForm() {
  const [conferences, setConferences] = useState([]);
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [conference, setConference] = useState("");
  const handleNameChange = (event) => {
    setName(event.target.value);
  }
  const handleEmailChange = (event) => {
    setEmail(event.target.value);
  }
  const handleConferenceChange = (event) => {
    setConference(event.target.value);
  }

  const fetchData = async () => {
    const urlConference = 'http://localhost:8000/api/conferences/';

    const responseConference = await fetch(urlConference);

    if (responseConference.ok) {
      const data = await responseConference.json();
      setConferences(data.conferences);
      //   for (let conference of data.conferences) {
      //     const option = document.createElement('option');
      //     option.value = conference.href;
      //     option.innerHTML = conference.name;
      //     selectTag.appendChild(option);
      //   }

      //     const loadingTag = document.getElementById('loading-conference-spinner');
      //     loadingTag.classList.add("d-none");
      //     selectTag.classList.remove("d-none");
    }
  }


  useEffect(() => { fetchData(); }, []);

  const resetState = () => {
    setEmail("");
    setName("");
    setConference("");

  }

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};

    data.name = name;
    data.email = email;
    data.conference = conference;

    console.log(data);
    resetState();



    const attendeeUrl = 'http://localhost:8001/api/attendees/';
    const fetchConfig = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      }
    }

    const response = await fetch(attendeeUrl, fetchConfig);
    if (response.ok) {
      const newAttendee = await response.json();

      setName('');
      setEmail('');
      setConference('');

    }

  }



  return (
    <div className="row">
      <div className="col col-sm-auto">
        <img width="300" className="bg-white rounded shadow d-block mx-auto mb-4"
          src="logo.svg" />
      </div>
      <div className="col">
        <div className="card shadow">
          <div className="card-body">
            <form onSubmit={handleSubmit} id="create-attendee-form">
              <h1 className="card-title">It's Conference Time!</h1>
              <p className="mb-3">
                Please choose which conference
                you'd like to attend.
              </p>
              {/* <div className="d-flex justify-content-center mb-3" id="loading-conference-spinner">
                  <div className="spinner-grow text-secondary" role="status">
                    <span className="visually-hidden">Loading...</span>
                  </div>
                </div> */}
              <div className="mb-3">
                <select onChange={handleConferenceChange} value={conference} name="conference" id="conference" className="form-select d-none" required>
                  <option value="">Choose a conference</option>
                  {conferences.map(conference => {
                    return (
                      <option key={conference.href} value={conference.href}>
                        {conference.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <p className="mb-3">
                Now, tell us about yourself.
              </p>
              <div className="row">
                <div className="col">
                  <div className="form-floating mb-3">
                    <input onChange={handleNameChange} value={name} required placeholder="Your full name" type="text" id="name" name="name" className="form-control" />
                    <label htmlFor="name">Your full name</label>
                  </div>
                </div>
                <div className="col">
                  <div className="form-floating mb-3">
                    <input onChange={handleEmailChange} value={email} required placeholder="Your email address" type="email" id="email" name="email" className="form-control" />
                    <label htmlFor="email">Your email address</label>
                  </div>
                </div>
              </div>
              <button className="btn btn-lg btn-primary">I'm going!</button>
            </form>
            <div className="alert alert-success d-none mb-0" id="success-message">
              Congratulations! You're all signed up!
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default AttendeeForm;
