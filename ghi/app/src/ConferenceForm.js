import React, { useEffect, useState } from 'react';

function ConferenceForm() {
  const [locations, setLocations] = useState([]);
  const [name, setName] = useState("");
  const [starts, setStarts] = useState("");
  const [ends, setEnds] = useState("");
  const [description, setDescription] = useState("");
  const [maxPresentations, setMaxPresentations] = useState("");
  const [maxAttendees, setMaxAttendees] = useState("");
  const [location, setLocation] = useState("");
  const handleNameChange = (event) => {
    setName(event.target.value);
  }
  const handleStartsChange = (event) => {
    setStarts(event.target.value);
  }
  const handleEndsChange = (event) => {
    setEnds(event.target.value);
  }
  const handleDescriptionChange = (event) => {
    setDescription(event.target.value);
  }
  const handleMaxPresentationsChange = (event) => {
    setMaxPresentations(event.target.value);
  }
  const handleMaxAttendeesChange = (event) => {
    setMaxAttendees(event.target.value);
  }
  const handleLocationChange = (event) => {
    setLocation(event.target.value);
  }

  const fetchData = async () => {
    const url = 'http://localhost:8000/api/locations/';

    const responseLocation = await fetch(url);

    if (responseLocation.ok) {
      const data = await responseLocation.json();
      setLocations(data.locations);
      // console.log(locations);
      // const selectTag = document.getElementById('state');
      // for (let state of data.states) {
      //   const option = document.createElement('option');
      //   option.value = state.abbreviation;
      //   option.innerHTML = state.name;
      //   selectTag.appendChild(option);
    }
  }


  useEffect(() => { fetchData(); }, []);

  const resetState = () => {
    setLocation("");
    setName("");
    setStarts("");
    setEnds("");
    setDescription("");
    setMaxPresentations("");
    setMaxAttendees("");

  }

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};

    data.name = name;
    data.starts = starts;
    data.ends = ends;
    data.description = description;
    data.max_presentations = maxPresentations; //needs to match model property name
    data.max_attendees = maxAttendees;
    data.location = location;

    console.log(data);
    // resetState();



    const conferenceUrl = 'http://localhost:8000/api/conferences/';
    const fetchConfig = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      }
    }

    const response = await fetch(conferenceUrl, fetchConfig);
    if (response.ok) {
      const newConference = await response.json();

      setName('');
      setStarts('');
      setEnds('');
      setDescription('');
      setMaxPresentations('');
      setMaxAttendees('');
      setLocation('');
    }

  }



  return (
    <div className='my-5 container'>
      {/* // <div className="row"> */}
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new conference</h1>
          <form onSubmit={handleSubmit} id="create-conference-form">
            <div className="form-floating mb-3">
              <input onChange={handleNameChange}
                required name="name"
                placeholder="Name"
                type="text"
                id="name"
                className="form-control"
                value={name}
              />
              <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleStartsChange}
                required name="starts"
                placeholder="Conference start date"
                type="date"
                id="starts"
                className="form-control"
                value={starts}
              />
              <label htmlFor="starts">Starts</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleEndsChange}
                value={ends}
                required name="ends"
                placeholder="Conference end date"
                type="date"
                id="ends"
                className="form-control"
              />
              <label htmlFor="ends">Ends</label>
            </div>
            <div className="mb-3">
              <label htmlFor="description">Description</label>

              <textarea onChange={handleDescriptionChange} required name="description" id="description" rows="3" className="form-control" value={description}></textarea>

            </div>
            <div className="form-floating mb-3">

              <input onChange={handleMaxPresentationsChange} required name="max_presentations" placeholder="Maximum presentations" type="number" id="max_presentations" className="form-control" value={maxPresentations} />
              <label htmlFor="max_presentations">Maximum Presentations</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleMaxAttendeesChange} required name="max_attendees" placeholder="Maximum attendees" type="number" id="max_attendees" className="form-control" value={maxAttendees} />
              <label htmlFor="max_attendees">Maximum Attendees</label>

            </div>
            <div className="mb-3">
              <select onChange={handleLocationChange} value={location} required name="location" id="location" className="form-select">
                <option value="">Choose a location</option>
                {locations.map(location => {
                  return (
                    <option key={location.id} value={location.id}>
                      {location.name}
                    </option>
                    // comes from line 18 option.value = location.id; of new-conference.js
                  );
                })}
              </select>

            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}



export default ConferenceForm;
