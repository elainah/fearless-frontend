import React, { useEffect, useState } from 'react';

function LocationForm () {
  const [states, setStates] = useState([]);
  const [name, setName] = useState("");
  const [roomCount, setRoomCount] = useState ("");
  const [city, setCity] = useState ("");
  const [state, setState] = useState("");
  const handleNameChange = (event) => {
    setName(event.target.value);
  }
  const handleRoomCountChange = (event) => {
    setRoomCount(event.target.value);
  }
  const handleCityChange = (event) => {
    setCity(event.target.value);
  }
  const handleStateChange = (event) => {
    setState(event.target.value);
  }



  const fetchData = async () => {
    const url = 'http://localhost:8000/api/states/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setStates(data.states);
      // const selectTag = document.getElementById('state');
      // for (let state of data.states) {
      //   const option = document.createElement('option');
      //   option.value = state.abbreviation;
      //   option.innerHTML = state.name;
      //   selectTag.appendChild(option);
    }
  }


  useEffect(() => {fetchData();}, []);

  const resetState = () => {
    setName("");
    setRoomCount("");
    setCity("");
    setState("");

  }

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data ={};

    data.room_count = roomCount;
    data.name = name;
    data.city = city;
    data.state = state;

    console.log(data);
    resetState();
  }



  return (
    <div className="row">
    <div className="offset-3 col-6">
      <div className="shadow p-4 mt-4">
        <h1>Create a new location</h1>
        <form onSubmit={handleSubmit} id="create-location-form">
          <div className="form-floating mb-3">
            <input onChange={handleNameChange} placeholder="Name" required type="text" name="name" value={name} id="name" className="form-control"/>
            <label htmlFor="name">Name</label>
          </div>
          <div className="form-floating mb-3">
            <input onChange={handleRoomCountChange} placeholder="Room count" required type="number" name="room_count" value={roomCount} id="room_count" className="form-control"/>
            <label htmlFor="room_count">Room count</label>
          </div>
          <div className="form-floating mb-3">
            <input onChange={handleCityChange} value={city} placeholder="City" required type="text" name="city" id="city" className="form-control"/>
            <label htmlFor="city">City</label>
          </div>
          <div className="mb-3">
            <select onChange={handleStateChange} required name="state" value={state} id="state" className="form-select">
              <option value="">Choose a state</option>
              {states.map(state => {
                return (
                  <option key={state.abbreviation} value={state.abbreviation}>
                    {state.name}
                  </option>
                );
              })}
            </select>
          </div>
          <button className="btn btn-primary">Create</button>
        </form>
      </div>
    </div>
  </div>
  );
}

export default LocationForm;
