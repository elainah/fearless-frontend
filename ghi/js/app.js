function createCard(name, description, pictureUrl,starts, ends, location) {
    return `
      <div class="card">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
          <div class="card-footer bg-gray "><small>${starts} - ${ends}</small></div>
        </div>
      </div>
    `
  }


  window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';
    const columns = document.querySelectorAll(".col");
    let colIndx = 0;

    try {
      const response = await fetch(url);

      if (!response.ok) {
        console.log("Bad response")


        // Figure out what to do when the response is bad

      } else {
        const data = await response.json();

        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const starts = new Date(details.conference.starts);
            const ends = new Date(details.conference.ends);
            const title = details.conference.name;
            const description = details.conference.description;
            const location = details.conference.location.name;
            const pictureUrl = details.conference.location.picture_url;
            const html = createCard(title, description, pictureUrl, starts.toLocaleDateString(), ends.toLocaleDateString(), location);
            // const column = document.querySelector('.col');
            const column = columns[colIndx % 3];
            column.innerHTML += html;
            colIndx = (colIndx +1) % 3;
          }
        }

      }
    } catch (e) {
      // Figure out what to do if an error is raised
      console.log("error, no data")

    }


});
