// Get the cookie out of the cookie store

    async function cookies(){
        const payloadCookie = await cookieStore.get("jwt_access_payload");
        // FINISH THIS
        if (payloadCookie) {

            const storedValue = payloadCookie.value;
    // The cookie value is a JSON-formatted string, so parse it
            const encodedPayload = JSON.parse(storedValue);
    // Convert the encoded payload from base64 to normal string
            const decodedPayload = atob(encodedPayload);
            console.log(decodedPayload)
    // The payload is a JSON-formatted string, so parse it
            const payload = JSON.parse(decodedPayload);
    // Print the payload
            console.log(payload)

            const conference = payload.user.perms;
            const permissionConf = conference.includes("events.add_conference");
            if (permissionConf) {
                const newConferenceNav = document.getElementById("navconference")
                newConferenceNav.classList.remove("d-none");
                console.log(newConferenceNav)
            }
            const location = payload.user.perms;
            const permissionLocation = location.includes("events.add_location");
            if (permissionLocation) {
                const newLocationNav = document.getElementById("navlocation");
                newLocationNav.classList.remove("d-none");
                console.log(newLocationNav)

            const presentation = payload.user.perms;
            console.log(presentation)

            const permissionPresentation = presentation.includes("presentations.add_presentation");
            if (permissionPresentation) {
                const newPresentationNav = document.getElementById("navPresentation");
                newPresentationNav.classList.remove("d-none");
            }
            }
        }
    }
