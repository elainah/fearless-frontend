
window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/states/';

    try {
      const response = await fetch(url);
      if (!response.ok) {
        // need to fix

      } else {
        const data = await response.json();
        console.log(data)

        const selectTag = document.getElementById("state");

        for (let state of data.states) {
            const option = document.createElement("option");
            option.value = state.abbreviation;
            option.innerHTML = state.name;
            selectTag.appendChild(option);

        }
      }



  //this section creates location form with event listener, when they push the button
    const formTag = document.getElementById('create-location-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();


        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        // this displays the data after you created it
        console.log(json)



        const locationUrl = 'http://localhost:8000/api/locations/';
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
            'Content-Type': 'application/json',
            },
        };

        const locationResponse = await fetch(locationUrl, fetchConfig);
        console.log(locationResponse)

        if (locationResponse.ok) {
            formTag.reset();
            const newLocation = await locationResponse.json();
            console.log(newLocation)
        }
        })
      } catch (e) {
      console.log("error, no data")
      }

  });
